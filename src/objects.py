from trainerbase.gameobject import GameByte, GameFloat, GameInt
from trainerbase.memory import Address, pm

from memory import coord_struct_pointer, heart_rate_pointer, player_state_pointer


xp = GameInt(pm.base_address + 0x74644C)
hp = GameFloat(Address(player_state_pointer, [0x44]))
world_interaction = GameByte(Address(player_state_pointer, [-0x23]))
heart_rate = GameFloat(Address(heart_rate_pointer, [0xC]))

coord_struct_address = Address(coord_struct_pointer, [0xC, 0x60])
player_x = GameFloat(coord_struct_address)
player_y = GameFloat(coord_struct_address + 0x8)
player_z = GameFloat(coord_struct_address + 0x4)
