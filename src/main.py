from trainerbase.main import run

from gui import run_menu
from injections import update_coord_struct_pointer, update_heart_rate_pointer, update_player_state_pointer


def on_initialized():
    update_player_state_pointer.inject()
    update_heart_rate_pointer.inject()
    update_coord_struct_pointer.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
