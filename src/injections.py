from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import pm

from memory import coord_struct_pointer, heart_rate_pointer, player_state_pointer


infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0x4E668B,
    """
        mov dword [ecx + 0x4], 99
        cmp byte [esi + 0xA2], 0
    """,
    original_code_length=9,
)

update_heart_rate_pointer = AllocatingCodeInjection(
    pm.base_address + 0x50E902,
    f"""
        mov [{heart_rate_pointer}], esi
        divss xmm1, [esi + 0xC]
        mov byte [esi + 0x4], 1
    """,
    original_code_length=9,
)

update_player_state_pointer = AllocatingCodeInjection(
    pm.base_address + 0x48983D,
    f"""
        mov dword eax, [eax + 0x8]
        comiss xmm0, [eax + ebx + 0x44]

        push edx
        mov edx, eax
        add edx, ebx
        mov [{player_state_pointer}], edx
        pop edx
    """,
    original_code_length=8,
)

update_coord_struct_pointer = AllocatingCodeInjection(
    pm.base_address + 0x42DE1F,
    f"""
        mov eax, [eax + 0x1A8]

        mov [{coord_struct_pointer}], ecx
    """,
    original_code_length=6,
)
