from trainerbase.memory import allocate_pointer


player_state_pointer = allocate_pointer()
heart_rate_pointer = allocate_pointer()
coord_struct_pointer = allocate_pointer()
