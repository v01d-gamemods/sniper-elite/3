from dearpygui import dearpygui as dpg
from trainerbase.gui import (
    CodeInjectionUI,
    GameObjectUI,
    SeparatorUI,
    SpeedHackUI,
    TeleportUI,
    add_components,
    simple_trainerbase_menu,
)

from injections import infinite_ammo
from objects import heart_rate, hp, world_interaction, xp
from teleport import tp


@simple_trainerbase_menu("Sniper Elite 3", 700, 315)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F1"),
                GameObjectUI(hp, "HP", "F2"),
                GameObjectUI(heart_rate, "Heart Rate", "F3"),
                GameObjectUI(xp, "XP"),
                GameObjectUI(world_interaction, "World Interaction"),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Teleport & Speedhack"):
            add_components(TeleportUI(tp))
